package com.jmr.ram.kabelo.jmr;

import com.jmr.ram.kabelo.jmr.model.RealEstateTransaction;
import com.jmr.ram.kabelo.jmr.service.interfaces.RealEstateTransactionService;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class JmrApplicationTests {

	@Autowired
	RealEstateTransactionService realEstateTransactionService;
	public String filename = "realestatetransactions_TEST.csv";
	Path path = Paths.get(filename);

	RealEstateTransaction transaction = RealEstateTransaction.builder()
			.street("3526 HIGH ST")
			.city("SACRAMENTO")
			.zip(95838)
			.state("CA")
			.beds(2)
			.baths(1)
			.sq__ft(836)
			.type("Residential")
			.sale_date("Wed May 21 00:00:00 EDT 2008")
			.price(59222)
			.latitude(38.631913)
			.longitude(-121.434879)
			.build();

	 void initAll()  {



		try {
			if(Files.exists(path)) {
				Files.delete(path);
			}

			realEstateTransactionService.setFileName(filename);
			List<RealEstateTransaction> list = new LinkedList<>();
			list.add(transaction);
			realEstateTransactionService.updateList(list);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@AfterAll
	void cleanUp() {
		realEstateTransactionService.setFileName(filename);

		try {
			if(Files.exists(path)) {
				Files.delete(path);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Test
	void testGetList() {

		cleanUp();
		initAll();
		try {
			List<RealEstateTransaction> list = realEstateTransactionService.getList();
			assertEquals(list.size(), 1);
			RealEstateTransaction record1 = list.remove(0);
			assertEquals(record1, transaction);

		} catch (Exception e) {
			fail("should not throw execption");

		}
	}

	@Test
	void testUpdateList() {

		cleanUp();
		initAll();

		try {
			List<RealEstateTransaction> list = realEstateTransactionService.getList();
			transaction.setBaths(55);
			list.add(transaction);
			realEstateTransactionService.updateList(list);

			//read the records again to ensure new record was added
			list = realEstateTransactionService.getList();
			assertEquals(list.size(), 2);
			RealEstateTransaction record2 = list.remove(1);
			assertEquals(record2, transaction);

		} catch (Exception e) {
			fail("should not throw execption");

		}
	}

}
