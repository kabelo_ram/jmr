package com.jmr.ram.kabelo.jmr.service.impl;

import com.jmr.ram.kabelo.jmr.model.CSVHeading;
import com.jmr.ram.kabelo.jmr.model.RealEstateTransaction;
import com.jmr.ram.kabelo.jmr.service.interfaces.RealEstateTransactionService;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;


@Service
public class RealEstateTransactionServiceImpl implements RealEstateTransactionService {

    private String fileName;

    @Override
    public List<RealEstateTransaction> getList() throws IOException, ParseException {

        Path path = Paths.get(fileName);

        if(Files.exists(path)){

            BufferedReader reader = Files.newBufferedReader(path);
            String line = reader.readLine();//read out the heading line

            //read in the file with the heading data,
            Iterable<CSVRecord> records = CSVFormat
                    .RFC4180.withHeader(CSVHeading.class)
                    .parse(reader);

            //build up the transaction object for each record in the csv file.

             List<RealEstateTransaction> listOfTransactions = StreamSupport.stream(records.spliterator(), false)
                    .map(r -> RealEstateTransaction.builder()
                                    .baths(Integer.parseInt(r.get(CSVHeading.baths)))
                                    .beds(Integer.parseInt(r.get(CSVHeading.beds)))
                                    .city(r.get(CSVHeading.city))
                                    .price(Integer.parseInt(r.get(CSVHeading.price)))
                                    .state(r.get(CSVHeading.state))
                                    .street(r.get(CSVHeading.street))
                                    .zip((Integer.parseInt(r.get(CSVHeading.zip))))
                                    .type(r.get(CSVHeading.type))
                                    .sq__ft(Integer.parseInt(r.get(CSVHeading.sq__ft)))
                                    .sale_date(r.get(CSVHeading.sale_date))
                                    .latitude((Double.parseDouble(r.get(CSVHeading.latitude))))
                                    .longitude((Double.parseDouble(r.get(CSVHeading.longitude))))
                                    .build()

                    ).collect(Collectors.toList());


             return listOfTransactions;

        }else{
            throw new RuntimeException("Could not find realEstateTransaction.csv");
        }

    }

    @Override
    public void updateList(List<RealEstateTransaction> listOfTransactions) throws IOException {

        BufferedWriter writer = Files.newBufferedWriter(Paths.get(fileName));

        CSVPrinter csvPrinter = new CSVPrinter(writer, CSVFormat.DEFAULT
                .withHeader(CSVHeading.class));

        //print out each object of transaction to the csv file
        listOfTransactions.forEach(record -> {
            try {
                csvPrinter.printRecord(record.getStreet(),
                        record.getCity(),
                        record.getZip(),
                        record.getState(),
                        record.getBeds(),
                        record.getBaths(),
                        record.getSq__ft(),
                        record.getType(),
                        record.getSale_date(),
                        record.getPrice(),
                        record.getLatitude(),
                        record.getLongitude()
                );
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

            csvPrinter.flush();


    }

    @Override
    public void setFileName(String name) {
        this.fileName = name;
    }

    @Override
    public String getFileName() {
        return this.fileName;
    }
}
