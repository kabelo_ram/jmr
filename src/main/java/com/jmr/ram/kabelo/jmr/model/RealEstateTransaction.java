package com.jmr.ram.kabelo.jmr.model;

import lombok.Builder;
import lombok.Data;


@Builder
@Data
/**
 * This class represents an individual transaction as shown in the csv file
 */
public class RealEstateTransaction {

    private String street;
    private String city;
    private int zip;
    private String state;
    private int beds;
    private int baths;
    private int sq__ft;
    private String	type;
    private String sale_date;
    private int price;
    private double latitude;
    private double longitude;
    private boolean editable = false;
}
