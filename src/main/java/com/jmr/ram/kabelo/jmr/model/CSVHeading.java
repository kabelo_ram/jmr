package com.jmr.ram.kabelo.jmr.model;

/**
 * This enum represents the headings of the csv file
 */
public enum CSVHeading {
    street, city, zip, state, beds, baths, sq__ft, type, sale_date, price, latitude, longitude
}
