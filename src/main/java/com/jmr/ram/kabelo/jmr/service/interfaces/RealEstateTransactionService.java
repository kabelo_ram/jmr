package com.jmr.ram.kabelo.jmr.service.interfaces;

import com.jmr.ram.kabelo.jmr.model.RealEstateTransaction;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;

public interface RealEstateTransactionService {

    List<RealEstateTransaction> getList() throws IOException, ParseException;
    void updateList(List<RealEstateTransaction> list) throws IOException;
    void setFileName(String name);
    String getFileName();

}
