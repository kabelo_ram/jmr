package com.jmr.ram.kabelo.jmr.boundry;

import com.jmr.ram.kabelo.jmr.model.RealEstateTransaction;
import com.jmr.ram.kabelo.jmr.service.interfaces.RealEstateTransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200")//allow requests from the angular app
public class RealEstateTransactionController {
    @Autowired
    RealEstateTransactionService realEstateTransactionService;

    @GetMapping("/transactions")
    public List<RealEstateTransaction> getList(){

        List<RealEstateTransaction> list = null;

        try {
            list = realEstateTransactionService.getList();
        } catch (Exception e) {
            e.printStackTrace();
            //return an empty list if an error occurs
        }

        return list;
    }

    @PutMapping("/transactions")
    public void updateList(@RequestBody List<RealEstateTransaction> listOfTransactions){

        try {
            realEstateTransactionService.updateList(listOfTransactions);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
